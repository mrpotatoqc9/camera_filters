""" 

Opencv computer vision: introduction
By: Patrick C-M
Date:  Mai 2020
Description:
gives a pencil style look to the recording of the camera
works great with built in laptop camera




finally got it ! after doing the pencil two diffrent ways i had to search for the real solution
that site helped me : https://www.freecodecamp.org/news/sketchify-turn-any-image-into-a-pencil-sketch-with-10-lines-of-code-cf67fa4f68ce/
I often had pretty good result while experimenting different thing but in general the picture and the background was always too gray... theres no good threshold algorithm for putting pixel to white when its below thresh and leave the rest untouched... maybe an adaptive threshold could be usefull in this situation since its very light sensitive, but ive already spent too much time on this to keep going on experimenting.
"""

import cv2


kernelSize = 21
#kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3,3))
cap = cv2.VideoCapture(0)

def dodge(front,back): 
	result=front*255/(255-back)  
	result[result>255]=255 
	result[back==255]=255 
	return result.astype('uint8')

while True:
	
	check, frame = cap.read()
	gray=cv2.GaussianBlur(frame,(3,3),0)
	gray=cv2.cvtColor(gray,cv2.COLOR_BGR2GRAY)
	gray2 =255-gray
	
	laplacian = cv2.Laplacian(gray, cv2.CV_32F, ksize = kernelSize,scale = -1, delta = 0)
	cv2.normalize(laplacian, 
                dst = laplacian, 
                alpha = 0, 
                beta = 255, 
                norm_type = cv2.NORM_MINMAX, 
                dtype = cv2.CV_32F)
	
	laplacian2= cv2.Laplacian(gray2, cv2.CV_32F, ksize = kernelSize,scale = 4, delta =0)
	cv2.normalize(laplacian2, 
                dst = laplacian2, 
                alpha = 0, 
                beta = 255, 
                norm_type = cv2.NORM_MINMAX, 
                dtype = cv2.CV_32F)

	
	laplacian3 = cv2.GaussianBlur(laplacian2,(9,9),0)
	laplacian4 = cv2.GaussianBlur(laplacian,(9,9),0)
	#laplacian = cv2.bilateralFilter(laplacian, 9, 80, 80)
	#laplacian2 = cv2.bilateralFilter(laplacian2, 9, 80, 80)

	#pencil = dodge(laplacian,laplacian2)
	pencilGauss = dodge(laplacian4,laplacian3)
	cv2.imshow('lap3',laplacian3)
	#cv2.imshow('lap',laplacian)
	#cv2.imshow('lap2',laplacian2)
	cv2.imshow('lap4',laplacian4)
	#cv2.imshow('result',pencil)
	cv2.imshow('result wit gauss',pencilGauss)
	if cv2.waitKey(25) & 0xFF == ord('q'):
		break

cap.release()
cv2.destroyAllWindows()

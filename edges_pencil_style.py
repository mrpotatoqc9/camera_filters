"""
Opencv computer vision: introduction
By: Patrick C-M
Date:  Mai 2020
Description:
gives a pencil style look to the recording of the camera (pencil_style.py is better)
works great with built in laptop camera
"""

import cv2
#import time


cap = cv2.VideoCapture(0)
gray2=None
framediff=None
while True:
	
	check, frame = cap.read()
	
	gray=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
	medianBlurred = cv2.medianBlur(gray,21)
	edges = cv2.Canny(gray,75,150)
	result=cv2.bitwise_not(edges)
	
	#cv2.imshow('thresh',threshdiff)
	#cv2.imshow('frame',frame)
	#cv2.imshow('capturing',gray)
	cv2.imshow('diff',result)
	
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

cap.release()
cv2.destroyAllWindows()


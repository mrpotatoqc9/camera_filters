"""
Opencv computer vision: introduction
By: Patrick C-M
Date:  Mai 2020
Description:
gives a cartoon style look to the recording of the camera
works great with built in laptop camera
"""
import cv2


kernelSize = 21
kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3,3))
cap = cv2.VideoCapture(0)

while True:
	
	check, frame = cap.read()
	gaussblur = cv2.GaussianBlur(frame,(3,3),0)
	gray=cv2.cvtColor(gaussblur,cv2.COLOR_BGR2GRAY)
	#gaussblur = cv2.GaussianBlur(frame,(3,3),0)
	#gray = cv2.medianBlur(gray,3)
	#gray = cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,9,9)
	laplacian = cv2.Laplacian(gray, cv2.CV_32F, ksize = kernelSize,scale = 200, delta = 100)
	cv2.normalize(laplacian, 
                dst = laplacian, 
                alpha = 0,
                beta = 255, 
                norm_type = cv2.NORM_MINMAX, 
                dtype = cv2.CV_32F)
	
	laplacian = laplacian.astype('uint8')
	gray = cv2.medianBlur(gray,21)
	#laplacian = cv2.adaptiveThreshold(laplacian,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,21,21)
	laplacian = 255-laplacian
	edges = cv2.Canny(laplacian,75,150)
	edges = cv2.dilate(edges,kernel)
	#edges = cv2.erode(edges,kernel)
	edges=cv2.bitwise_not(edges)
	#edges = cv2.cvtColor(edges,cv2.COLOR_GRAY2BGR)
	result = cv2.bitwise_and(frame,frame,mask=edges)

	
	cv2.imshow('lap',laplacian)
	cv2.imshow('edges',edges)
	cv2.imshow('result',result)


	if cv2.waitKey(25) & 0xFF == ord('q'):
		break

cap.release()
cv2.destroyAllWindows()

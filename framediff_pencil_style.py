"""
Opencv computer vision: introduction
By: Patrick C-M
Date:  Mai 2020
Description:
gives a pencil style look to the recording of the camera (pencil_style.py is better)
works great with built in laptop camera
"""

import cv2
import time

kernelSize = 21
kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3,3))
cap = cv2.VideoCapture(0)
gray2=None
lap2=None
while True:
	
	check, frame = cap.read()
	gray=cv2.GaussianBlur(frame,(9,9),0)
	gray=cv2.cvtColor(gray,cv2.COLOR_BGR2GRAY)
	
	gray2 =255-gray
	laplacian = cv2.Laplacian(gray, cv2.CV_32F, ksize = kernelSize,scale = 5, delta =0)
	#laplacian2= cv2.Laplacian(gray, cv2.CV_32F, ksize = kernelSize,scale = -1000, delta =0)
	cv2.normalize(laplacian, 
                dst = laplacian, 
                alpha = 0, 
                beta = 1, 
                norm_type = cv2.NORM_MINMAX, 
                dtype = cv2.CV_32F)
	laplacian2= cv2.Laplacian(gray2, cv2.CV_32F, ksize = kernelSize,scale = 1, delta =0)
	cv2.normalize(laplacian2,
                dst = laplacian2, 
                alpha = 0, 
                beta = 1, 
                norm_type = cv2.NORM_MINMAX, 
                dtype = cv2.CV_32F)

	laplacian = cv2.bilateralFilter(laplacian, 9, 80, 80)	
	#laplacian2 = laplacian2 & 0x0F
	laplacian2 = cv2.bilateralFilter(laplacian2, 9, 80, 80)
	#dst,laplacian2 = cv2.threshold(laplacian2,240,255,cv2.THRESH_TRUNC)
	result =cv2.absdiff(laplacian,laplacian2)
	result = 1-result
	cv2.imshow('gray',gray)
	cv2.imshow('lap',laplacian)
	cv2.imshow('lap2',laplacian2)
	cv2.imshow('gray2',gray2)
	cv2.imshow('result',result)
	#cv2.imshow('negative',negative)
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

cap.release()
cv2.destroyAllWindows()


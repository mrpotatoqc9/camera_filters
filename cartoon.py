"""
Opencv computer vision: introduction
By: Patrick C-M
Date:  Mai 2020
Description:
gives a cartoon style look to the recording of the camera
works great with built in laptop camera
"""
import cv2


kernelSize = 21
kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3,3))
cap = cv2.VideoCapture(0)

while True:
	
	check, frame = cap.read()
	gray=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
	#gray = cv2.medianBlur(gray,21)

	
	edges = cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,21,21)
	edges = cv2.erode(edges,kernel)
	#edges = cv2.dilate(edges,kernel)
	#edges = cv2.erode(edges,kernel)
	result = cv2.bitwise_and(frame,frame,mask=edges)

	
	cv2.imshow('gray',gray)
	cv2.imshow('edges',edges)
	cv2.imshow('result',result)


	if cv2.waitKey(25) & 0xFF == ord('q'):
		break

cap.release()
cv2.destroyAllWindows()
